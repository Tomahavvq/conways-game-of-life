#include "data.h"
#include "rules.h"

#include <stdio.h>
#include <stdlib.h>

cell_map rules (cell_map game_map, cell_map environs)
{
	int i,j;
	for (i=0; i<game_map->n; i++) {
		for (j=0; j<game_map->m; j++) {
			if (game_map->map[i][j] == 0 && environs->map[i][j] == 3) {
				game_map->map[i][j] = 1;
			}
			else if (game_map->map[i][j] == 1 && environs->map[i][j] != 2 && environs->map[i][j] != 3) {
				game_map->map[i][j] = 0;
			}
		}
	}

	return game_map;
}
