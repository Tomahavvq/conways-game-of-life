#include "data.h"

#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

cell_map cell_map_alloc(int n, int m) 
{
	int i;
	cell_map matrix;
	matrix = malloc(sizeof *matrix);
	if (!matrix) {
		return NULL;
	}
	matrix->map = malloc(n * sizeof(int *));
	if (!matrix->map) {
		free(matrix);
		return NULL;
	}
	for (i=0; i<n; i++) {
		matrix->map[i] = malloc (m * sizeof (int));
		if (!matrix->map[i]) {
			free(matrix->map);
			free(matrix);
		}
	}
	
	return matrix;
}

cell_map cell_map_read (FILE *in) 
{
	int row, column;
	int i, j;
	cell_map matrix;

	if (fscanf (in, "%d %d", &row, &column) != 2) {
		return NULL;
	}

	if ((matrix = cell_map_alloc(row, column)) == NULL) {
		return NULL;
	}
	matrix->n = row;
	matrix->m = column;
	for (i=0; i<row; i++) {
		for (j=0; j<column; j++) {
			if (fscanf (in, "%d", &matrix->map[i][j]) != 1) {
				return NULL;
			}
			if (matrix->map[i][j] >= 1) matrix->map[i][j] = 1;
			else matrix->map[i][j] = 0;
		}
	}
	return matrix;
}

void cell_map_write(FILE *outr, cell_map matrix)
{
	int i, j;
	fprintf(outr, "%d %d\n", matrix->n, matrix->m);
	for (i=0; i<matrix->n; i++) {
		for (j=0; j<matrix->m - 1; j++) {
			fprintf(outr, "%d ", matrix->map[i][j]);
		}
		fprintf(outr, "%d\n", matrix->map[i][matrix->m - 1]);
	}
}


void cell_map_destroy(cell_map hangman)
{
	int i;
	for (i=0; i<hangman->n; i++) {
		free(hangman->map[i]);
	}
	free(hangman->map);
	free(hangman);
}

