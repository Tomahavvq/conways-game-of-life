#include "environs.h"
#include "data.h"

#include <stdio.h>
#include <stdlib.h>

cell_map environs (cell_map game_map)
{
	int i,j,tmpi,tmpi1,tmpj,tmpj1,s = 0;
	cell_map env;
	env = cell_map_alloc (game_map->n, game_map->m);
	env->n = game_map->n;
	env->m = game_map->m;
	if (!env) {	
		return NULL;
	}
	for (i=0; i<env->n; i++) {
		for (j=0; j<env->m; j++) {	
			s = 0;
			if (i == 0) {
				tmpi = env->n;
				tmpi1 = i;
			}
			else if (i == env->n-1) { 
					tmpi = i;
					tmpi1 = -1;
			} else {
				tmpi = i;
				tmpi1 = i;
			}	
			if (j == 0) { 
				tmpj = env->m;
				tmpj1 = j;
			}
			else if (j == env->m-1) {
				tmpj = j;
				tmpj1 = -1;
			} else { 
				tmpj = j;
				tmpj1 = j;
			}
			if (game_map->map[tmpi-1][tmpj1+1]) s++;	
			if (game_map->map[i][tmpj1+1]) s++;	
			if (game_map->map[tmpi1+1][tmpj1+1]) s++;	
			if (game_map->map[tmpi-1][j]) s++;	
			if (game_map->map[tmpi1+1][j]) s++;	
			if (game_map->map[tmpi1+1][tmpj-1]) s++;	
			if (game_map->map[i][tmpj-1]) s++;	
			if (game_map->map[tmpi-1][tmpj-1]) s++;
			
			env->map[i][j] = s;	
		}
	}
/*	
	s = 0;
	if (game_map->map[1][0]) s++;
	if (game_map->map[1][1]) s++;
	if (game_map->map[0][1]) s++;
	env->map[0][0] = s;

	s = 0;
	if (game_map->map[0][game_map->m-2]) s++;
	if (game_map->map[1][game_map->m-2]) s++;
	if (game_map->map[1][game_map->m-1]) s++;
	env->map[0][game_map->m-1] = s;

	s = 0;
	if (game_map->map[game_map->n-2][0]) s++;
	if (game_map->map[game_map->n-2][1]) s++;
	if (game_map->map[game_map->n-1][1]) s++;
	env->map[game_map->n-1][0] = s;
	
	s = 0;
	if (game_map->map[game_map->n-2][game_map->m-1]) s++;
	if (game_map->map[game_map->n-2][game_map->m-2]) s++;
	if (game_map->map[game_map->n-1][game_map->m-2]) s++;
	env->map[env->n-1][env->m-1] = s;

	for (i=1; i<env->m-1; i++) {
		s = 0;
		if (game_map->map[0][i-1]) s++;
		if (game_map->map[0][i+1]) s++;
		if (game_map->map[1][i-1]) s++;
		if (game_map->map[1][i]) s++;
		if (game_map->map[1][i+1]) s++;
		env->map[0][i] = s;
	}
	
	for (i=1; i<env->m-1; i++) {
		s = 0;
		if (game_map->map[game_map->n-1][i-1]) s++;
		if (game_map->map[game_map->n-1][i+1]) s++;
		if (game_map->map[game_map->n-2][i-1]) s++;
		if (game_map->map[game_map->n-2][i]) s++;
		if (game_map->map[game_map->n-2][i+1]) s++;
		env->map[env->n-1][i] = s;
	}
	
	for (i=1; i<env->n-1; i++) {
		s = 0;
		if (game_map->map[i-1][0]) s++;
		if (game_map->map[i+1][0]) s++;
		if (game_map->map[i-1][1]) s++;
		if (game_map->map[i][1]) s++;
		if (game_map->map[i+1][1]) s++;
		env->map[i][0] = s;
	}

	for (i=1; i<env->n-1; i++) {
		s = 0;
		if (game_map->map[i-1][env->m-1]) s++;
		if (game_map->map[i+1][env->m-1]) s++;
		if (game_map->map[i-1][env->m-2]) s++;
		if (game_map->map[i][env->m-2]) s++;
		if (game_map->map[i+1][env->m-2]) s++;
		env->map[i][env->m-1] = s;
	}*/
	return env;	
}
