#include "control.h"

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

char *usage =
  "Usage: %s -i input-file [-o current-generation-file] -n number-of-generation [-a path-for-PNG -k number-of-PNG]  \n"
  "	       n must be given, n >= 1\n"	
  "            if current-generation-file is given then\n"
  "               writes current cells generation to current-generation-file\n"
  "            endif\n"
  "            if path-for-PNG is given then\n"
  "               makes k PNG pictures of current generation\n"
  "               - k number-of-PNG must be >= 1\n"
  "            endif\n";

int main (int argc, char **argv)
{
  int opt;
  char *in = NULL;
  char *outr = NULL;
  char *outpng = NULL;
  int n = 0;
  int k = 0;
	char *progname= argv[0];

  /* process options, save user choices */
  while ((opt = getopt (argc, argv, "i:o:n:a:k:")) != -1) {
    switch (opt) {
    case 'i':
      in = optarg;
      break;
    case 'o':
      outr = optarg;
      break;
    case 'n':
      n = atoi (optarg);
      break;
    case 'a':
      outpng = optarg;
      break;
    case 'k':
      k = atoi (optarg);
      break;
    default:                   /* '?' */
      fprintf (stderr, usage, progname);
      exit (EXIT_FAILURE);
    }
  }
	if( optind < argc ) {
		fprintf( stderr, "\nBad parameters!\n" );
		for( ; optind < argc; optind++ )
			fprintf( stderr, "\t\"%s\"\n", argv[optind] );
		fprintf( stderr, "\n" );
		fprintf( stderr, usage, progname );
		exit( EXIT_FAILURE );
	}

	if( in == NULL || n < 1) {
		fprintf( stderr, "\n" );
		fprintf( stderr, usage, progname );
		exit( EXIT_FAILURE );
	} 
	
	if( outpng != NULL && k < 1 ) {
		fprintf( stderr, "\n" );
		fprintf( stderr, usage, progname );
		exit( EXIT_FAILURE );
	}
	control (in, outr, outpng, n, k);
	
	return 0;
}
