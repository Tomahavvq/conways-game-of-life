#ifndef _DATA_H_
#define _DATA_H_

#include <stdio.h>

typedef struct u {
	int n,m;
	int **map;
} *cell_map;

cell_map cell_map_alloc(int n, int m);
cell_map cell_map_read (FILE *in);
void cell_map_write(FILE *outr, cell_map matrix);
void cell_map_destroy(cell_map hangman);

#endif
