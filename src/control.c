#include "control.h"
#include "data.h"
#include "game_engine.h"
#include "image.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
void control (char *in, char *outr, char *outpng, int n, int k)
{
	FILE *input, *output;
	int i;
	char str[100], tmp[6];
	input = fopen(in, "r");
	if (!input) {
		fprintf(stderr, "Cannot read input file: %s\n", in);
		exit (EXIT_FAILURE);
	}

	cell_map game_t;
	game_t = cell_map_read(input);
	fclose(input);
	if (!game_t) {
		fprintf(stderr, "Cannot read file\n");
		exit (EXIT_FAILURE);
	}
	for (i=0; i<n; i++) {
		game_t = game (game_t);
		
		if (outr) {
			output = fopen(outr, "w");
			if (!output) {
				fprintf(stderr, "Cannot read output file: %s\n", outr);
				exit (EXIT_FAILURE);
			}
			cell_map_write(output, game_t);
			fclose(output);
			if (k > 0) {
				strcpy(str,outpng);
				sprintf(tmp,"%d",i);
				strcat(str,tmp);
				make_image (game_t, str);
				k--;
			}
		}
	}
	cell_map_destroy(game_t);
}
