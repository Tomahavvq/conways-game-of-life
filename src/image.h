#ifndef _IMAGE_H_
#define _IMAGE_H_
#include "data.h"
void encodeOneStep(const char* filename, const unsigned char* image, unsigned width, unsigned height);
void make_image (cell_map game_map, char *outpng);
#endif
