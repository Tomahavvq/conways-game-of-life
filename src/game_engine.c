#include "game_engine.h"
#include "data.h"
#include "environs.h"
#include "rules.h"

#include <stdio.h>
#include <stdlib.h>

cell_map game(cell_map game_map) 
{
	cell_map env;
	env = environs(game_map);
	if (!env) {
		fprintf(stderr, "Cannot allocate cell_map\n");
		exit( EXIT_FAILURE);	
	}
	game_map = rules (game_map, env);
	cell_map_destroy (env);

	return game_map;
}
