#include "data.h"
#include "image.h"
#include "lodepng.h"
#include <stdio.h>
#include <stdlib.h>


void encodeOneStep(const char* filename, const unsigned char* image, unsigned width, unsigned height)
{
  /*Encode the image*/
  unsigned error = lodepng_encode32_file(filename, image, width, height);

  /*if there's an error, display it*/
  if(error) printf("error %u: %s\n", error, lodepng_error_text(error));
}


void make_image (cell_map game_map, char *outpng)
{
  /*generate some image*/
  unsigned width = game_map->n, height = game_map->m;
  unsigned char* image = malloc(width * height * 4);
  unsigned x, y;
  unsigned char tmp;
  for(y = 0; y < height; y++)
  	for(x = 0; x < width; x++) {
    		if (game_map->map[x][y] == 1) {
			tmp = 0;
		} else {
			tmp = 255;
		}
		image[4 * width * y + 4 * x + 0] = tmp;
    		image[4 * width * y + 4 * x + 1] = tmp;
    		image[4 * width * y + 4 * x + 2] = tmp;
    		image[4 * width * y + 4 * x + 3] = 255;
  	}

  encodeOneStep(outpng, image, width, height);

  free(image);
}
