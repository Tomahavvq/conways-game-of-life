CFLAGS=-g -O2 -Wall -Wextra -Isrc -rdynamic -DNDEBUG $(OPTFLAGS)

SOURCES=$(wildcard src/*.c) 
OBJECTS=$(patsubst %.c,%.o,$(SOURCES)) 
TARGET =bin/life
 
# Build 
all: bin $(TARGET) 
 
dev: CFLAGS=-g -Wall -Isrc -Wall -Wextra $(OPTFLAGS) 
dev: all 

$(TARGET): $(OBJECTS) 
	$(CC) -o $(TARGET) $(OBJECTS) 
 
 
bin: 
	@mkdir -p bin 
#The cleaner
clean:
	rm -rf bin $(OBJECTS) tests/*.out

#The checker 
BADFUNCS='[^_.>a-zA-Z0-9](str(n?cpy|n?cat|xfrm|n?dup|str|pbrk|tok|_)|stpn?cpy|a?sn?printf|byte_)' 
check: 
	@echo Files with potentially dangerous functions. 
	@egrep $(BADFUNCS) $(SOURCES) || true
